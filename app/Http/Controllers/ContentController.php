<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Comment;
use Redirect;
use Auth;
use DB;
use Njasm\Soundcloud\SoundcloudFacade;
use Htmldom;
use App\User;
use Input;


class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = content::all();

        return View('content.index', compact($content));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.contentAdd');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $content = new Content();
        $url = parse_url(Request::get('url'));
        $url2 = parse_url(Request::get('url'));
        $url3 = Request::get('url');


        $type_explode = explode('.', $url['host']);




        if($type_explode{'1'} == 'youtube'){
            $url_explode = explode('.', $url2['query']);
            $url_movie = $url_explode['0'];
            $url_final = substr($url_movie, 2,11);
            $content->type = $type_explode{'1'};
            $content->url = "https://www.youtube.com/embed/".$url_final;

            //$content_title = file_get_contents("http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=".$url_final);
            //parse_str($content_title, $ytarr);
            //$content->title = $ytarr['title'];

            $jfo = new \Htmldom("http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=".$url_final);
            $data = json_decode($jfo, TRUE);
                $youtube_title = $data['title'];

            $content->title=$youtube_title;

        }

        elseif($type_explode{'0'} == 'soundcloud'){
            $content->type = $type_explode{'0'};

            $jfo = new \Htmldom("https://api.soundcloud.com/resolve.json?url=".$url3."&client_id=57627cf9511fb7f549e8b77935d91200");
            $data = json_decode($jfo, TRUE);

            $soundcloud_id = $data['id'];
            $soundcloud_title =$data['title'];


            $url_soundcloud = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/".$soundcloud_id;
            $content->url = $url_soundcloud;
            $content->title = $soundcloud_title;

        }

        elseif($type_explode{'0'} == 'vimeo'){
            $content->type = $type_explode{'0'};

            /*
            $vimeo_id= substr(parse_url($url3, PHP_URL_PATH), 1);
            $vimeo_url = "https://player.vimeo.com/video/".$vimeo_id;*/

            $urlParts = explode("/", parse_url($url3, PHP_URL_PATH));
            $videoId = (int)$urlParts[count($urlParts)-1];
            $vimeo_url = "https://player.vimeo.com/video/".$videoId;

            $hash = json_decode(file_get_contents("http://vimeo.com/api/v2/video/{$videoId}.json"));
            $vimeo_title = $hash[0]->title;

            $content->title = $vimeo_title;
            $content->url = $vimeo_url;



        }
        else{
            //$jfo = new \Htmldom("http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=".$url_final);
            //$data = json_decode($jfo, TRUE);
            //$youtube_title = $data['title'];

            $content->url = $url3;
            $url_parse = parse_url($url3, PHP_URL_PATH);

            $content->title = $url_parse;

        }



        //$content->url =Request::get('url');
        $content->user_id = Auth::user()->id;
        $content->save();
        return Redirect::to("/content");

    }
//$content->url =Request::get('url');

    public function showdashboard()
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $content = Content::find($id);
        $user = User::find($content->user_id);
        $comment =  Comment::where('user_content', '=', $content->id)->get();
        /* $comment = DB::table('comments')
             ->where('user_content', $id)
             ->join('users', 'users.id', '=', 'comments.user_id')
             ->select('users.*', 'comments.*')
             ->get();*/
        //$comment=  Comment::where('user_content', '=', $content->id)->get();







        return view("content.detail", compact('content', 'user', 'comment'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function comments(Request $request, $id){
        $user = Auth::user();
        $comment = new comment();
        if(null !== Auth::user()){
            $comment->user_id = $user->id;

        }
        else{

            $comment->user_id = null;
        }
        $comment->user_content = $id;
        $comment->body = Request::get('body');


        $comment->save();

        return redirect()->route('content.detail', [$id]);

    }






    public function destroy($id)
    {
        //
    }
}