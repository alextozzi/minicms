<?php
use App\Task;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/dashboard',['middleware' => 'auth','as'=>'index', function(){


    $content = DB::table('content')
        ->select('content.url', 'content.type')
        ->orderBy('created_at','desc')
        ->get();

   return view('/dashboard' , compact('content'));
}]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


//Content add



Route::resource('content', 'ContentController');

Route::get('content', function() {

    $content = DB::table('content')
        ->select('content.*')
        ->orderBy('created_at','desc')
        ->get();

    return view('content.index' , compact('content'));
});


Route::get('content/create',['middleware' => 'auth','as' =>'store', function(){

    return view('content.contentAdd');
}]);

/**
 * Display All Tasks
 */
Route::get('/task', function () {
    $tasks = DB::table('tasks')
        ->select('tasks.*')
        ->get();
    return view('tasks',[
        'tasks' => $tasks
    ]);

});

/**
 * Add A New Task
 *
 *  $tasks = Task::orderBy('created_at', 'asc')->get();

return view('tasks', [
'tasks' => $tasks
]);
 *
 */
Route::post('/task', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $task = new Task;
    $task->name = $request->name;
    $task->save();

    return redirect('/task');
});


Route::post('/task/{id}', function (Request $request,$id){
        Task::find($id);
    $selectOption = $_POST['status'];
        $task->status = $request->$selectOption;
    return redirect('/task');
});



/**
 * Delete An Existing Task
 */
Route::delete('/task/{id}', function ($id) {
    Task::findOrFail($id)->delete();

    return redirect('/task');
});


// COmment section detail page

Route::post('content/comment/{id}', [
    'as' => 'comment.save',
    'uses' => 'ContentController@comments'
]);




Route::get('content/{id}', [
    'as' => 'content.detail',
    'uses' => 'ContentController@show'
]);