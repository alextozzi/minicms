<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = "content";

    protected  $fillable = ['url','type','user_id','title'];

    public function user(){
        $this->belongsTo("App\User","User_id","id");
    }
}
