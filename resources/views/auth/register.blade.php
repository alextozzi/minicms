<!-- resources/views/auth/register.blade.php -->
@extends('layouts.master')
@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif





<div class="container">
    <div class="row centered-form">
        {!! csrf_field() !!}
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign up </h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="POST" action="/auth/register">
                        {!! csrf_field() !!}

                        <div class="form-group">
                                    <input type="text" name="name" id="name" value="{{ old('name') }}"  class="form-control input-sm" placeholder="Name">
                                </div>


                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control input-sm" value="{{ old('email') }}" placeholder="Email Address">
                        </div>

                        <div class="form-group">
                            <input type="code" name="code" id="code" class="form-control input-sm"  placeholder="Registration Code">
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="Register" class="btn btn-info btn-block">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


    @endsection