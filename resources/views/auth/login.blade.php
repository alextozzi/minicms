<!-- resources/views/auth/login.blade.php -->
@extends('layouts.master')

@section('content')

<form method="POST" action="/auth/login" class="form-signin">
    {!! csrf_field() !!}
    <h2 class="form-signin-heading">Please sign in</h2>
    <div>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" class="form-control" name="email" placeholder="Email address" required autofocus value="{{ old('email') }}">
    </div>
<br>
    <div>
        <label for="inputPassword" class="sr-only" >Password</label>

        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
    </div>
<br>
    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </div>
</form>

    @endsection