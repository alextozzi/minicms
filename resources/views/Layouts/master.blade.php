<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

</head>
<body>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="/content">Minicms List</a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    @if(null !== Auth::user())

                        <li><a href="/auth/logout">Sign Out</a></li>
                        <li><a href="/content/create">Add Content</a></li>
                        <li><a href="/task">Todo Listt</a></li>
                    @else
                        <li><a href="/auth/login">Log In</a></li>
                        <li><a href="/auth/register">Register</a></li>
                        <li><a href="/content/create">Add Content</a></li>
                        <li><a href="/task">Todo Listt</a></li>
                    @endif

                </ul>
            </div>
        </div>
    </nav>
</div>



<div class="col-sm-offset-2 col-sm-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            </div>
        @yield('content')
        </div>
    </div>





</body>
</html>