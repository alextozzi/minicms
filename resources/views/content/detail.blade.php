@extends('layouts.master')
@section('content')

<div class="container">
    <div class="col-sm-offset-2 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>{{$content->title}}</h1>
                </div>

            <iframe style="margin-left:-100px;" width="800px" height="500px;" src="{{$content->url}}"></iframe>

            <div class="panel-body">
                <h2>Comments</h2>
                {!! Form::open(array('route' => array('comment.save', $content->id),'method'=>'POST')) !!}

                <div class="form-group">
                    {!! Form::label('body', 'Leave a comment', ['class' => 'control-label']) !!}

                    {!! Form::textarea('body', null, array('class' => 'form-control', 'required')) !!}
                </div>

                <div class="form-group">
                    <div class="col-md-7 col-md-offset-4">
                        <a href="/content" class="send-btn btn-lg btn-primary borderless" style="margin-right:30px;">Back </a>
                        {!! Form::submit('Post comment', array('class'=>'send-btn btn-lg btn-primary borderless')) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                    </div>
            <div class="panel-body">
                @foreach($comment as $c)
                    <div class="detail-comment" >
                        <p>
                          @if($c->user_id !== null)
                                <strong>{{$c->user->name}}</strong>  {!! nl2br(e($c->body)) !!}

                           @else
                                <strong>Anonymous:</strong>  {!! nl2br(e($c->body)) !!}

                              @endif
                        </p>
                    </div>
                @endforeach

            </div>



            </div>
        </div>
    </div>

@endsection



